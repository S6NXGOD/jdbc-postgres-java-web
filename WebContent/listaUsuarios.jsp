<%@page import="org.apache.tomcat.util.security.MD5Encoder"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.Import"%>
<%@page import="java.util.List"%>
<%@page import="br.com.exemplo.beans.Usuario"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Lista de Usu�rios</title>
</head>
<body>
	<jsp:include page="cabecalho.jsp"></jsp:include>
</body>

<%
	List<Usuario> listaResultado = (List<Usuario>)request.getAttribute("lista");

%>
<table border="1">
	<tr bgcolor="#eaeaea">
		<th>ID</th>
		<th>Nome</th>
		<th>Email</th>
		<th>Senha</th>
		<th>Excluir</th>
		<th>Alterar</th>
	</tr>

<%
	for (Usuario u:listaResultado) {

%>
	<tr>
		<th><%= u.getId() %></th>
		<th><%= u.getNome() %></th>
		<th><%= u.getEmail() %></th>
		<th><%= u.getSenha()%></th>
		<th><a href="UsuarioControl?acao=ex&id=<%= u.getId() %>">Excluir</a></th>
		<th><a href="UsuarioControl?acao=alt&id=<%= u.getId() %>">Alterar</a></th>
	</tr>
<%
	}
%>
</table>
</html>