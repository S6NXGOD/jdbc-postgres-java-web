<%@page import="br.com.exemplo.beans.Usuario"%>
<%@page import="br.com.exemplo.JDBC.UsuarioDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="cabecalho.jsp"></jsp:include>
	
	<%
		Usuario user = (Usuario)request.getAttribute("usuario");
		
	%>
	<form action="UsuarioControl" method="post">
		<label>ID:</label>
		<input size="5" type="text" name="id" required="required" value="<%=user.getId()%>">
		<label>Nome:</label>
		<input type="text" name="nome" required="required" value="<%=user.getNome()%>">
		<label>Email:</label>
		<input type="email" name="email" required="required" value="<%=user.getEmail()%>">
		<label>Senha:</label>
		<input type="password" name="senha" required="required" placeholder="Digite uma nova senha">
		
		<input type="submit" value="Alterar"> 
	</form>	
</body>
</html>