<%@page import="br.com.exemplo.JDBC.UsuarioDAO"%>
<%@page import="br.com.exemplo.beans.Usuario"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cadastro Salvo!</title>
</head>
<body>
	<jsp:include page="cabecalho.jsp"></jsp:include>
	<%
	String snome = request.getParameter("nome");
	String semail = request.getParameter("email");
	String sSenha = request.getParameter("senha");
	
	Usuario user = new Usuario();
	user.setNome(snome);
	user.setEmail(semail);
	user.setSenha(sSenha);
	
	UsuarioDAO userDAO = new UsuarioDAO();
	userDAO.cadastroUsuario(user);
	
	%>
	<h1>Salva com sucesso!</h1>
</body>
</html>