package br.com.exemplo.JDBC;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.exemplo.beans.Usuario;

public class UsuarioDAO {
	
	private Connection con = Conexao.getConnection();
	
	public void cadastroUsuario(Usuario usuario) {
		
		String sql = "INSERT INTO usuario (nome,email,senha) VALUES (?,?,md5(?))";
		
		try {
			PreparedStatement preparador = con.prepareStatement(sql);
			preparador.setString(1, usuario.getNome());
			preparador.setString(2, usuario.getEmail());
			preparador.setString(3, usuario.getSenha());
			
			preparador.execute();
			preparador.close();
			System.out.println("Cadastrado com Sucesso!");
		}catch(SQLException e){
			System.out.println("Erro - " + e.getMessage());
		}
	}
	public void AlterarUsuario(Usuario usuario) {
		
		String sql = "UPDATE usuario SET nome=?,email=?,senha=md5(?) where id=?";
		
		try {
			PreparedStatement preparador = con.prepareStatement(sql);
			preparador.setString(1, usuario.getNome());
			preparador.setString(2, usuario.getEmail());
			preparador.setString(3, usuario.getSenha());
			preparador.setInt(4, usuario.getId());
			
			preparador.execute();
			preparador.close();
			System.out.println("Alterado com Sucesso!");
		}catch(SQLException e){
			System.out.println("Erro - " + e.getMessage());
		}
	}
	public void DeletarUsuario(Usuario usuario) {
		
		String sql = "DELETE FROM usuario where id=?";
		
		try {
			PreparedStatement preparador = con.prepareStatement(sql);
			preparador.setInt(1, usuario.getId());
			
			preparador.execute();
			preparador.close();
			System.out.println("Deletado com Sucesso!");
		}catch(SQLException e){
			System.out.println("Erro - " + e.getMessage());
		}
	}
	public List<Usuario> buscarTodos(Usuario usuario){
		String sql = "select * from usuario";
		
		List<Usuario> lista = new ArrayList<Usuario>();
		try {
			PreparedStatement preparador = con.prepareStatement(sql);
			ResultSet resultado = preparador.executeQuery();
			while (resultado.next()) {
				Usuario user = new Usuario();
				user.setId(resultado.getInt("id"));
				user.setNome(resultado.getString("nome"));
				user.setEmail(resultado.getString("email"));
				user.setSenha(resultado.getString("senha"));
				lista.add(user);
				}
		}catch(SQLException e) {
			System.out.println("Error - " + e.getMessage());
		}return lista;	
	}
	public Usuario buscaPorID(Integer id) {
		Usuario usuRetorno = null;
		String sql = "Select * from usuario where id=?";
		
		try {
			PreparedStatement preparador = con.prepareStatement(sql);
			preparador.setInt(1, id);
			ResultSet resultado = preparador.executeQuery();
			
			if (resultado.next()) {
				usuRetorno = new Usuario();
				usuRetorno.setId(resultado.getInt("id"));
				usuRetorno.setNome(resultado.getString("nome"));
				usuRetorno.setEmail(resultado.getString("email"));
				usuRetorno.setSenha(resultado.getString("senha"));
			}
			System.out.println("Econtrado com sucesso!");
		}catch(SQLException e){
			System.out.println("Error - " + e.getMessage());
		}return usuRetorno;
	}
	public Usuario autenticacao(Usuario usuario) {
		Usuario usuRetorno = null;
		String sql = "Select * from usuario where email = ? and senha = md5(?)";
		
		try {
			PreparedStatement preparador = con.prepareStatement(sql);
			preparador.setString(1, usuario.getEmail());
			preparador.setString(2, usuario.getSenha());
			ResultSet resultado = preparador.executeQuery();
			
			if (resultado.next()) {
				usuRetorno = new Usuario();
				usuRetorno.setId(resultado.getInt("id"));
				usuRetorno.setNome(resultado.getString("nome"));
				usuRetorno.setEmail(resultado.getString("email"));
				usuRetorno.setSenha(resultado.getString("senha"));
			}
			System.out.println("Econtrado com sucesso!");
		}catch(SQLException e){
			System.out.println("Error - " + e.getMessage());
		}return usuRetorno;
	}
}

