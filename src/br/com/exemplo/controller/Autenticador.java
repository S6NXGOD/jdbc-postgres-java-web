package br.com.exemplo.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.exemplo.JDBC.UsuarioDAO;
import br.com.exemplo.beans.Usuario;

/**
 * Servlet implementation class Autenticador
 */

@WebServlet("/Autenticador")
public class Autenticador extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public Autenticador() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String semail = request.getParameter("email");
		String ssenha = request.getParameter("senha");
		
		Usuario user = new Usuario();
		user.setEmail(semail);
		user.setSenha(ssenha);
		
		UsuarioDAO userDAO = new UsuarioDAO();
		Usuario userAutenticado = userDAO.autenticacao(user);
		
		
		if (userAutenticado!=null) {
			request.getRequestDispatcher("home.jsp").forward(request, response);
		}else {
			response.sendRedirect("erroLogin.jsp");
		}
	}

}
