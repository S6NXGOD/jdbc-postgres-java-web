package br.com.exemplo.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.postgresql.util.MD5Digest;

import br.com.exemplo.JDBC.UsuarioDAO;
import br.com.exemplo.beans.Usuario;

/**
 * Servlet implementation class UsuarioControl
 */
@WebServlet("/UsuarioControl")
public class UsuarioControl extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsuarioControl() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Usuario user = new Usuario();
		String acao = request.getParameter("acao");
		UsuarioDAO userDAO = new UsuarioDAO();
		
		if(acao != null && acao.equals("lis")) {
			
		List<Usuario> lista = userDAO.buscarTodos(user);
		
		request.setAttribute("lista", lista);
		RequestDispatcher saida = request.getRequestDispatcher("listaUsuarios.jsp");
		saida.forward(request, response);
	}else if(acao != null && acao.equals("ex")){
		String id = request.getParameter("id");
		user.setId(Integer.parseInt(id));
		userDAO.DeletarUsuario(user);
		
		response.sendRedirect("UsuarioControl?acao=lis");
	}else if(acao != null && acao.equals("alt")) {
		String id = request.getParameter("id");
		Usuario usuario = userDAO.buscaPorID(Integer.parseInt(id));
		request.setAttribute("usuario", usuario);
		request.getRequestDispatcher("alterarCadastro.jsp").forward(request, response);
	}
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//par�metros da tela
		
		String sid = request.getParameter("id");
		String snome = request.getParameter("nome");
		String semail = request.getParameter("email");
		String ssenha = request.getParameter("senha");
		
		//criando objeto usuario e atribuindo valores a tela
		
		Usuario usuario = new Usuario();
		usuario.setNome(snome);
		usuario.setEmail(semail);
		usuario.setSenha(ssenha);
		usuario.setId(Integer.parseInt(sid));
		
		//criando um usu�rio DAO
		
		UsuarioDAO usuarioDAO = new UsuarioDAO();
		//salvando no banco de dados
		usuarioDAO.AlterarUsuario(usuario);
		response.sendRedirect("UsuarioControl?acao=lis");
		
	}

}
