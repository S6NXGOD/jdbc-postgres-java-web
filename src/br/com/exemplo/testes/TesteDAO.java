package br.com.exemplo.testes;
import java.util.List;

import br.com.exemplo.JDBC.UsuarioDAO;
import br.com.exemplo.beans.Usuario;

public class TesteDAO {

	public static void main(String[] args) {
//		testeCadastro();
//		testeAlterar();
//		testeDeletar();
		testeBuscarTodos();
	}
	
	public static void testeCadastro() {
		Usuario user = new Usuario();
		user.setNome("J.Pedro");
		user.setEmail("joaopedro@gmail.com");
		user.setSenha("Senha1234");
		
		UsuarioDAO userDao = new UsuarioDAO();
		
		userDao.cadastroUsuario(user);
	}
	
	public static void testeAlterar() {
		Usuario user = new Usuario();
		user.setNome("Jo�o Pedro Pinto do �");
		user.setEmail("JP@gmail.com");
		user.setSenha("Senha4321");
		user.setId(7);
		
		UsuarioDAO userDao = new UsuarioDAO();
		
		userDao.AlterarUsuario(user);
	}
	
	public static void testeDeletar() {
		Usuario user = new Usuario();
		
		user.setId(16);
		
		UsuarioDAO userDao = new UsuarioDAO();
		userDao.DeletarUsuario(user);
	}
	public static void testeBuscarTodos() {
		Usuario user = new Usuario();
		UsuarioDAO userDAO = new UsuarioDAO();
		List<Usuario> listaResultado = userDAO.buscarTodos(user);
		
		for (Usuario u:listaResultado) {
			System.out.println("ID : " + u.getId() + " / Nome : " + u.getNome() + " / Email : " + u.getEmail());
		}
		
	}
}
